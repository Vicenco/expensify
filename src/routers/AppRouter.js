import React from 'react';
import {createBrowserHistory} from 'history';

import { Router, Route, Switch } from 'react-router-dom';
import ExpenseDashboardPage from '../views/ExpenseDashboardPage';
import AddExpensePage from '../views/AddExpensePage';
import EditExpensePage from '../views/EditExpensePage';
import NotFoundPage from '../views/NotFoundPage';
import Header from '../components/header/Header';
import Footer from '../components/footer/Footer';
import LoginPage from '../views/LoginPage.view';
import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';
import RegisterPage from '../views/RegisterPage.view';

export const history = createBrowserHistory();

const AppRouter = () => (
  <Router history={history}>
    <div className="app">
      <Header />
      <Switch>
        <PublicRoute path="/" component={LoginPage} exact={true} />
        <PublicRoute path="/register" component={RegisterPage} />
        <PrivateRoute path="/dashboard" component={ExpenseDashboardPage} />
        <PrivateRoute path="/create" component={AddExpensePage} />
        <PrivateRoute path="/edit/:id" component={EditExpensePage} />
        <Route component={NotFoundPage} />
      </Switch>
      <Footer />
    </div>
  </Router>
);

export default AppRouter;