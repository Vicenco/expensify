import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { firebase } from './firebase/firebase';

import AppRouter, { history } from './routers/AppRouter';
import configureStore from './store/configureStore';
import Spinner from './components/spinner/Spinner';
import { startSetExpenses } from './actions/expenses';
import { startSetIncome } from './actions/income';
import { login, logout, setUserInfo } from './actions/auth';

import 'normalize.css/normalize.css';
import './styles/styles.scss';

const store = configureStore();

const jsx = (
  <Provider store={store}>
    <AppRouter />
  </Provider>
);

const renderApp = () => {
  ReactDOM.render(jsx, document.getElementById('root'));
};
ReactDOM.render(<Spinner />, document.getElementById('root'));


firebase.auth().onAuthStateChanged((user) => {

  if (user) {

    store.dispatch(setUserInfo(user));
    store.dispatch(login(user.uid));
    store.dispatch(startSetIncome());
    store.dispatch(startSetExpenses()).then(() => {

      renderApp();
      if (history.location.pathname === '/') {
        history.push('/dashboard');
      }

    });

  } else {

    store.dispatch(logout());
    renderApp();
    history.push('/');

  }

});