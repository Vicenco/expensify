export default (state = {}, action) => {

  switch (action.type) {

    case 'LOGIN':
      return {
        ...state,
        uid: action.uid
      };

    case 'USER_INFO':
      return {
        ...state,
        user: action.user
      };

    case 'LOGOUT':
      return {};

    default:
      return state;
  }
  
};
