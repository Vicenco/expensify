// Income Reducer
const incomeReducerDefaultState = [];

export default (state = incomeReducerDefaultState, action) => {

  switch (action.type) {

    case 'ADD_INCOME':
      return action.income;
    
    case 'SET_INCOME':
      return action.income;

    case 'CLEAR_INCOME':
      return state = "";

    default:
      return state;

  }
};
