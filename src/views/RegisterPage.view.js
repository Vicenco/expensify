import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import md5 from "md5";

import { startLogin } from '../actions/auth';
import { firebase } from '../firebase/firebase';
import '../styles/base/loginpage.styles.scss';


const RegisterPage = ({ startLogin }) => {

    const [errors, setErrors] = useState([]);
    const [newUser, setnewUser] = useState({ username: '', passwordConfirmation: '', email: '', password: '' });
    let usersRef = firebase.database().ref("users");

    /**
     * Handles change event for input fields.
     * 
     * @param {event} e - Field parameters triggered by change event.
     * @private
     */
    const onValueChange = (e) => {

        const sValue = e.target.value;
        setnewUser({ ...newUser, [e.target.name]: sValue });
        setErrors([]);

    };

    const displayErrors = errors => errors.map((error, i) => <p key={i}>{error.message}</p>);


    /**
     * Form submission.
     * 
     * @param {event} e - Triggered by user submit press.
     * @private
     */
    const handleSubmit = event => {

        event.preventDefault();

        if (isFormValid()) {

            setErrors([]);
            firebase
                .auth()
                .createUserWithEmailAndPassword(newUser.email, newUser.password)
                .then(createdUser => {

                    createdUser.user
                        .updateProfile({
                            displayName: newUser.username,
                            photoURL: `http://gravatar.com/avatar/${md5(createdUser.user.email)}?d=identicon`
                        })
                        .then(() => {
                            saveUser(createdUser);
                        })
                        .catch(err => {
                            setErrors(errors.concat(err));
                        });

                })
                .catch(err => {
                    console.error(err);
                    setErrors(errors.concat(err));
                });
        }

    };


    const isFormValid = () => {

        let errors = [];
        let error;

        if (isFormEmpty(newUser)) {

            error = { message: "Fill in all fields" };
            setErrors(errors.concat(error));
            return false;

        } else if (!isPasswordValid(newUser)) {

            error = { message: "Password is invalid" };
            setErrors(errors.concat(error));
            return false;

        } else {
            return true;
        }
    };

    const isFormEmpty = ({ username, email, password, passwordConfirmation }) => {
        return (
            !username.length ||
            !email.length ||
            !password.length ||
            !passwordConfirmation.length
        );
    };

    const isPasswordValid = ({ password, passwordConfirmation }) => {
        if (password.length < 6 || passwordConfirmation.length < 6) {
            return false;
        } else if (password !== passwordConfirmation) {
            return false;
        } else {
            return true;
        }
    };

    const handleInputError = (errors, inputName) => {
        return errors.some(error => error.message.toLowerCase().includes(inputName))
            ? "formControl errorInput"
            : "formControl ";
    };

    const saveUser = createdUser => {
        return usersRef.child(createdUser.user.uid).set({
            name: createdUser.user.displayName,
            avatar: createdUser.user.photoURL
        });
    };

    return (
        <div className="loginScreen">
            <div className="wrapper">
                <div className="left"></div>
                <div className="right">

                    {
                        errors.length > 0 ? (
                            <div className="signInErrorBox">
                                <h3>Error</h3>
                                {displayErrors(errors)}
                            </div>
                        ) : null
                    }

                    <div className="signinWrapper">
                        <h3>Register an Expensify account</h3>
                        <div className="authUser">
                            <form onSubmit={(e) => handleSubmit(e)}>
                                <input
                                    type="text"
                                    name="username"
                                    className={handleInputError(errors, "username")}
                                    placeholder="username"
                                    autoFocus
                                    value={newUser.username}
                                    onChange={(e) => onValueChange(e)}
                                />
                                <input
                                    type="text"
                                    name="email"
                                    className={handleInputError(errors, "email")}
                                    placeholder="Email address"
                                    autoFocus
                                    value={newUser.email}
                                    onChange={(e) => onValueChange(e)}
                                />
                                <input
                                    type="password"
                                    name="password"
                                    className={handleInputError(errors, "password")}
                                    placeholder="Password"
                                    value={newUser.password}
                                    onChange={(e) => onValueChange(e)}
                                />
                                <input
                                    type="password"
                                    name="passwordConfirmation"
                                    className={handleInputError(errors, "passwordConfirmation")}
                                    placeholder="Confirm password"
                                    value={newUser.passwordConfirmationword}
                                    onChange={(e) => onValueChange(e)}
                                />
                                <button>Submit</button>
                            </form>
                        </div>
                        <div className="registerBox">
                            Have an account already? <Link to="/">Sign in</Link>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    );
};

const mapDispatchToProps = (dispatch) => ({
    startLogin: () => dispatch(startLogin())
});

export default connect(undefined, mapDispatchToProps)(RegisterPage);