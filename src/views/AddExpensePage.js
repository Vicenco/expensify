import React from 'react';
import { connect } from 'react-redux';
import { startAddExpense } from '../actions/expenses';
import ExpenseForm from '../components/expenseForm/ExpenseForm';
import PageHeader from '../components/pageHeader/PageHeader';

const AddExpensePage = (props) => {

  const onSubmit = (expense) => {
    props.startAddExpense(expense);
    props.history.push("/")
  };

  return (
    <>
      <PageHeader title="Add Expense" />
      <div className="container">
        <ExpenseForm onFormSubmit={(e) => onSubmit(e)} />
      </div>
    </>
  )
}

const mapDispatchToProps = (dispatch) => ({
  startAddExpense: (expense) => {
    return (
      dispatch(startAddExpense(expense)) 
    )
}});

export default connect(undefined, mapDispatchToProps)(AddExpensePage);
