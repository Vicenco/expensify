import React from 'react';
import { connect } from 'react-redux';
import ExpenseForm from '../components/expenseForm/ExpenseForm';
import PageHeader from '../components/pageHeader/PageHeader';
import { startEditExpense, startRemoveExpense } from '../actions/expenses';

const EditExpensePage = ({ expense, history, match, startEditExpense }) => {

  const onSubmit = (expense) => {
    startEditExpense(match.params.id, expense);
    history.push("/")
  };

  return (
    <>
      <PageHeader title="Edit Expense" />
      <div className="container">
        <ExpenseForm
          expenseId={match.params.id}
          expenseData={expense}
          onFormSubmit={(e) => onSubmit(e)} />
      </div>
    </>
  );

};

const mapStateToProps = (state, props) => ({
  expense: state.expenses.find((expense) => expense.id === props.match.params.id)
});

const mapDispatchToProps = (dispatch, props) => ({
  startEditExpense: (id, expense) => dispatch(startEditExpense(id, expense)),
  startRemoveExpense: (data) => dispatch(startRemoveExpense(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(EditExpensePage);