import React from 'react';
import ExpenseList from '../components/expenseList/ExpenseList';
import ExpenseListFilters from '../components/expenseListFilters/ExpenseListFilters';
import ExpenseSummary from '../components/expenseSummary/ExpenseSummary';
import { withToastProvider, useToast } from '../components/toastMessage';

const ExpenseDashboardPage = () => {

  const toast = useToast();
  const showToast = () => toast.add('Expense has been removed');

  return (

    <div>
      <ExpenseSummary />
      <ExpenseListFilters />
      <ExpenseList toast={showToast} />
    </div>

  )
};

const AppWithToastProvider = withToastProvider(ExpenseDashboardPage);
export default AppWithToastProvider;
