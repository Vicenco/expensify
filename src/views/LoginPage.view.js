import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { startLogin } from '../actions/auth';

import { firebase } from '../firebase/firebase';
import '../styles/base/loginpage.styles.scss';


const LoginPage = ({ startLogin }) => {

    const [errors, setErrors] = useState([]);
    const [authUser, setAuthUser] = useState({ email: '', password: '' });

    
    /**
     * Handles change event for input fields.
     * 
     * @param {event} e - Field parameters triggered by change event.
     * @private
     */
    const onValueChange = (e) => {

        const sValue = e.target.value;
        setAuthUser({ ...authUser, [e.target.name]: sValue });
        setErrors([]);

    };

    const displayErrors = errors => errors.map((error, i) => <p key={i}>{error.message}</p>);


    /**
     * Form submission.
     * 
     * @param {event} e - Triggered by user submit press.
     * @private
     */
    const handleSubmit = event => {

        event.preventDefault();
        setErrors([]);

        firebase
        .auth()
        .signInWithEmailAndPassword(authUser.email, authUser.password)
        .catch(err => {
            console.error(err);
            setErrors([...errors, err]);
        });

    };

    
    const handleInputError = (errors, inputName) => {
        return errors.some(error => error.message.toLowerCase().includes(inputName))
            ? "formControl errorInput"
            : "formControl ";
    };

    return (
        <div className="loginScreen">
            <div className="wrapper">
                <div className="left"></div>
                <div className="right">
                   
                    { 
                        errors.length > 0 ? (
                            <div className="signInErrorBox">
                                <h3>Error</h3>
                                { displayErrors(errors) }
                            </div>
                        ) : null 
                   }
                
                    <div className="signinWrapper">
                        <h3>Login to Expensify</h3>
                        <div className="authUser">
                            <form onSubmit={(e) => handleSubmit(e)}>
                                <input
                                    type="text"
                                    name="email"
                                    className={handleInputError(errors, "email")}
                                    placeholder="Email address"
                                    autoFocus
                                    value={authUser.email}
                                    onChange={(e) => onValueChange(e)}
                                />
                                <input
                                    type="password"
                                    name="password"
                                    className={handleInputError(errors, "password")}
                                    placeholder="Password"
                                    value={authUser.password}
                                    onChange={(e) => onValueChange(e)}
                                />
                                <button>Submit</button>
                            </form>
                        </div>
                        <div className="registerBox">
                            Don't have an account? <Link to="/register">Register</Link>
                        </div>
                        <div className="registerBox" style={{ textAlign: 'center'}}>
                            <span className="link" onClick={startLogin}>Login with a Google account</span>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    );
};

const mapDispatchToProps = (dispatch) => ({
    startLogin: () => dispatch(startLogin())
});

export default connect(undefined, mapDispatchToProps)(LoginPage);