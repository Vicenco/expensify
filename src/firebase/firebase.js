import * as firebase from 'firebase';

const config = {
  apiKey: "AIzaSyB0xhO7LMPpyBqhcbVKl34EZEKfXX2fLf0",
  authDomain: "expensify-38472.firebaseapp.com",
  databaseURL: "https://expensify-38472.firebaseio.com",
  projectId: "expensify-38472",
  storageBucket: "expensify-38472.appspot.com",
  messagingSenderId: "555809785450",
  appId: "1:555809785450:web:84e62391a2c1939df9ec13",
  measurementId: "G-8J52JJNB2X"
};

firebase.initializeApp(config);

const database = firebase.database();
const googleAuthProvider = new firebase.auth.GoogleAuthProvider();

export { firebase, googleAuthProvider, database as default };