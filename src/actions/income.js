import database from '../firebase/firebase';

// ADD_INCOME
export const addIncome = (income) => ({
  type: 'ADD_INCOME',
  income
});

export const startAddIncomeAmount = (income) => {

  return (dispatch, getState) => {

    const uid = getState().auth.uid;    
    return database.ref(`users/${uid}/income`).set({
        income: income
      }).then((ref) => {
      
      dispatch(addIncome(income));

    });

  };

};


// SET_INCOME
export const setIncome = (income) => ({
  type: 'SET_INCOME',
  income
});


// CLEAR_INCOME
export const clearIncome = () => ({
  type: 'CLEAR_INCOME'  
});


export const startSetIncome = () => {
  return (dispatch, getState) => {
    
    const uid = getState().auth.uid;
    const fireDB = database;

    return fireDB.ref(`users/${uid}/income`).once('value').then((snapshot) => {

      let income = "";
      snapshot.forEach((childSnapshot) => {
        income = childSnapshot.val();
      });
      
      // income = income !== '' ? income : "000"
      dispatch(setIncome(income));

    });

  };

};