import React from 'react';
import ReactDOM from 'react-dom';
import './modal.styles.scss';

const Modal = ({ title, children, onClose, action, actionBtnLabel}) => {

    return ReactDOM.createPortal (
        <div className="dimmer">
            <div className="modal">
                <h2>{title}</h2>
                <div className="content">{children}</div>
                <div className="actions">
                    <button className="toggle-button" onClick={action}>{actionBtnLabel}</button>
                    <button className="toggle-button" onClick={onClose}>Cancel</button>
                </div>
            </div>
        </div>,
        document.querySelector('#modal')

    );

};

export default Modal;