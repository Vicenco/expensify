import React, { useState } from 'react';
import { DateRangePicker } from 'react-dates';

import { connect } from 'react-redux';
import { setTextFilter, sortByAmount, sortByDate, setStartDate, setEndDate } from '../../actions/filters';
import MonthPicker from '../monthPicker/MonthPicker';

import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css'
import './expenseListFilters.styles.scss';

const ExpenseListFilters = ({ dispatch, filters, expenses }) => {

    const [focused, setfocused] = useState(null);
    const [showMonthPicker, setDisplayMonthPicker] = useState(false);
    
    
    /**
     * Sets the selected date.
     * 
     * @param {date} createdAt - Selected date by user.
     * @prviate
     */
    const onDatesChange = ({ startDate, endDate }) => {

        dispatch(setStartDate(startDate));
        dispatch(setEndDate(endDate));

    };


    /**
     * Changes the focus of the Date range picker.
     * 
     * @param {date} focused - provides date value.
     * @private
     */
    const onFocusChange = (focused) => {
        setfocused(focused);
    };


    /**
     * Handles the input change request.
     * @param {event} oEvent - triggered by user change.
     * @private
     */
    const handleTextFieldChange = (oEvent) => {        
        dispatch(setTextFilter(oEvent.target.value));
    };


    /**
     * Handles the select change request.
     * @param {event} oEvent - triggered by user change.
     * @private
     */
    const handleSelectChange = (oEvent) => {
        
        const sValue = oEvent.target.value;
        switch(sValue){

            case 'amount':
                dispatch(sortByAmount());
                break;

            case 'date':
                dispatch(sortByDate());
                break;

            default:
                return null;
        }
        
    };
    
    const toggleMonthPicker = (e) => {
        e.preventDefault();
        setDisplayMonthPicker(!showMonthPicker);
    };

    const filterHistory = (date) => {
        
        const newEndDate = date.clone();

        dispatch(setStartDate(date.startOf('month')));
        dispatch(setEndDate(newEndDate.endOf('month')));
        setDisplayMonthPicker(false);

    };

    return (

        <div className="container filterList noselect">
            <div className="filterInputs">
                <div className="descBox">
                    <label htmlFor="title">Filter expense list</label>
                    <input name="title" className="formControl" type="text" placeholder="Expense name" value={filters.text} onChange={(e) => handleTextFieldChange(e) } />
                </div>  
                <div className="fieldPickers">
                            
                    <select className="formControl" value={filters.sortBy} onChange={(e) => handleSelectChange(e) }>
                        <option value="date">Date</option>
                        <option value="amount">Amount</option>
                    </select>
                    
                    <DateRangePicker
                        startDate={filters.startDate}
                        endDate={filters.endDate}
                        onDatesChange={(e) => onDatesChange(e)}
                        focusedInput={focused}
                        onFocusChange={(e) => onFocusChange(e)}
                        showClearDates={true}
                        numberOfMonths={1}
                        isOutsideRange={() => false}
                        startDateId="start"
                        endDateId="end"
                    />
                </div>
            </div>
            <div className="historyFilter">
                <button className="btn btn-green" onClick={(e) => toggleMonthPicker(e)}>View history</button>
            </div>
            <MonthPicker show={showMonthPicker} callBack={(e) => filterHistory(e)} data={expenses} />

    </div>

    );

};

const mapStateToProps = (state) =>({
    filters: state.filters,
    expenses: state.expenses
});
  
export default connect(mapStateToProps)(ExpenseListFilters);