import React, { useEffect, useRef } from 'react';

function Toast({ children, remove }) {

    const removeRef = useRef();
    removeRef.current = remove;

    useEffect(() => {

        const duration = 2000;
        const id = setTimeout(() => removeRef.current(), duration);
        return () => clearTimeout(id);

    }, []);

    return (
        <div className="toast">
            <div className="toast__text">
                {children}
            </div>
        </div>
    );
}

export default Toast;