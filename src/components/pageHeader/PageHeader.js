import React from 'react';
import './pageHeader.styles.scss';

const PageHeader = ({title}) => {
    return (
        <div className="pageHeader">
            <div className="container">
                <h1 className="title">{title}</h1>
            </div>
        </div>
    );
};

export default PageHeader;