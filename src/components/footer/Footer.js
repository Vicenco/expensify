import React from 'react';

const Footer = () => {
    return (
        <footer className="container noselect">
          <span>&copy; Vicenco 2020</span>
        </footer>
    );
};

export default Footer;