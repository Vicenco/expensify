import React, { useState, useEffect } from 'react';
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import numeral from "numeral";
import selectedExpenses from "../../selectors/expenses";
import selectedExpensesTotal from "../../selectors/expensesTotal";
import {startAddIncomeAmount} from '../../actions/income';

import Modal from '../modal/Modal';
import './expenseSummary.styles.scss';

export const ExpensesSummary = ({ expenseCount, expensesTotal, startAddIncomeAmount, incomeAmount }) => {

    const expenseWord = expenseCount === 1 ? 'expense' : 'expenses';
    const [showModal, setShowModal] = useState(false);
    const [income, setIncome] = useState("");
    const [errors, setErrors] = useState([]);
    const [incomeTotal, setIncomeAmount] = useState(incomeAmount);


    useEffect(() => {

        if (incomeAmount !== incomeTotal) {
            setIncomeAmount(incomeAmount);
        }
        
    }, [incomeAmount]);

    
    const toggleModal = (e) => {
        e.preventDefault();
        setShowModal(!showModal);
    };

    const formatNumValue = (sNumValue) => {
        return numeral(sNumValue / 100).format('$0,0.00')
    };


    /**
     * Handles change event for input fields.
     * 
     * @param {event} e - Field parameters triggered by change event.
     * @private
     */
    const onValueChange = (e) => {

        const sValue = e.target.value;
        setIncome(formatNumValue(sValue));
        setErrors([]);

    };

    const displayErrors = errors => errors.map((error, i) => <p key={i}>{error.message}</p>);


    /**
     * Form submission.
     * 
     * @param {event} e - Triggered by user submit press.
     * @private
     */
    const addIncomeAmount = event => {

        event.preventDefault();

        if (isFormValid()) {

            setErrors([]);
            startAddIncomeAmount((parseInt(income) + parseInt(incomeTotal ? incomeTotal : 0)));
            toggleModal(event);

        }

    };


    const isFormValid = () => {

        let errors = [];
        let error;

        if (isFormEmpty(income)) {

            error = { message: "Fill in all fields" };
            setErrors(errors.concat(error));
            return false;

        } else {
            return true;
        }

    };


    const isFormEmpty = (income) => {
        return (
            !income.length
        );
    };


    const handleInputError = (errors, inputName) => {
        return errors.some(error => error.message.toLowerCase().includes(inputName))
            ? "formControl errorInput"
            : "formControl ";
    };


    const setCurrentBalance = () => {
        
        let balanceTotal = incomeTotal - expensesTotal;
        return formatNumValue(balanceTotal);

    }

    return (
        <>
            <div className="summary noselect">
                <div className="container content">
                    <div className="columnLeft">
                        <h1 className="summaryTitle">Viewing <span>{expenseCount}</span> {expenseWord} totalling <span>{formatNumValue(expensesTotal)}</span></h1>
                        <div className="actions">
                            <Link className="btn" to="/create">Add Expense</Link>
                            <button className="btn btn-green" onClick={(e) => toggleModal(e)}>Add income</button>
                        </div>
                    </div>
                    <div className="columnRight">
                        <div className="amount">
                            <h3>Income</h3>
                            <span>{formatNumValue(incomeTotal)}</span>
                        </div>
                        <div className="amount">
                            <h3>Balance</h3>
                            <span className={ setCurrentBalance().includes("-") ? 'balanceDebt' : 'balanceNormal' }>{setCurrentBalance()}</span>
                        </div>
                    </div>
                </div>
            </div>
            {
                showModal ? (
                    <Modal 
                        title="Add income" 
                        show={true} 
                        onClose={(e) => toggleModal(e)} 
                        action={(e) => addIncomeAmount(e)} 
                        actionBtnLabel="Add income"
                    >
                        <div className="incomeContent">
                            <label htmlFor="income" text="Income" />
                            <input
                                type="text"
                                name="income"
                                className={handleInputError(errors, "income")}
                                placeholder="Add income amount"
                                value={income}
                                onChange={(e) => onValueChange(e)}
                            />
                        </div>
                    </Modal>
                ) : null
            }
        </>
    );

};

const mapStateToProps = (state) => {

    const visibleExpenses = selectedExpenses(state.expenses, state.filters);
    return {
        expenseCount: visibleExpenses.length,
        expensesTotal: selectedExpensesTotal(visibleExpenses),
        incomeAmount: state.income
    };

};

const mapDispatchToProps = (dispatch) => ({
    startAddIncomeAmount: (data) => dispatch(startAddIncomeAmount(data))
});
export default connect(mapStateToProps, mapDispatchToProps)(ExpensesSummary)
