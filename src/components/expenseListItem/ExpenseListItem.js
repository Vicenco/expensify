import React, { useState } from 'react';
import numeral from "numeral";
import moment from 'moment';

import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { startRemoveExpense } from '../../actions/expenses';

import Modal from '../modal/Modal';
import './expenseListItem.scss';

// load a locale
numeral.register('locale', 'gbp', {
    delimiters: {
        thousands: ',',
        decimal: '.'
    },
    abbreviations: {
        thousand: 'k',
        million: 'm',
        billion: 'b',
        trillion: 't'
    },
    ordinal: function (number) {
        return number === 1 ? 'GBP' : 'gbp';
    },
    currency: {
        symbol: '£'
    }
});

numeral.locale('gbp');

const ExpenseListItem = ({ dispatch, id, description, amount, createdAt, note, startRemoveExpense, showToast }) => {

    const [showModal, setShowModal] = useState(false);

    const removeExpenseItem = (e) => {
        
        e.preventDefault();
        startRemoveExpense({ id });

        toggleModal(e); 
        showToast();

    };

    const toggleModal = (e) => {
        e.preventDefault();
        setShowModal(!showModal);
    };

    return (
        <>
            <Link to={`/edit/${id}`} className="listItem">

                <div className="typeName">
                    <h3>{description}</h3>
                    <span>{note}</span>
                </div>

                <div className="endCol">
                    <div className="dueOn">
                        <span className="amount">{numeral(amount / 100).format('$0,0.00')} </span>
                        <span>{moment(createdAt).format('MMM Do, YYYY')}</span>
                    </div>
                    <div className="removeItemCol">
                        <button><FontAwesomeIcon icon={faTimes} onClick={(e) => toggleModal(e)} /></button>
                    </div>
                </div>

            </Link>
            {
                showModal ? (
                <Modal 
                    title="Remove expense" 
                    onClose={(e)=>toggleModal(e)} 
                    action={(e) => removeExpenseItem(e) }
                    actionBtnLabel="Ok"
                >
                    Are you sure you wish to remove this expense?
                </Modal>
                ) : null
            }
        </>
    );

};

const mapDispatchToProps = (dispatch, props) => ({
    startRemoveExpense: (data) => dispatch(startRemoveExpense(data))
});

export default connect(null,mapDispatchToProps)(ExpenseListItem);