import React, { useState } from 'react';
import { SingleDatePicker } from 'react-dates';
import moment from 'moment';

import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css'
import './expenseform.styles.scss';

const ExpenseForm = ({ expenseData, expenseId, onFormSubmit }) => {

    const [focused, setfocused] = useState(false);
    const [error, setError] = useState(null);
    const [expense, setExpense] = useState({
        description: expenseData ? expenseData.description : '',
        amount: expenseData ? (expenseData.amount / 100).toString() : '',
        createdAt: expenseData ? moment(expenseData.createdAt) : moment(),
        note: expenseData ? expenseData.note : ''
    });


    /**
     * Handles change event for input, select and textarea fields.
     * 
     * @param {event} e - Field parameters triggered by change event.
     * @private
     */
    const onValueChange = (e) => {

        const sValue = e.target.value;

        if (e.target.name === 'amount') {

            // Numbers and one decimal used to a maximum of a two decimal place.
            if (!sValue || sValue.match(/^\d{1,}(\.\d{0,2})?$/)) {
                setExpense({ ...expense, [e.target.name]: sValue });
                setError(null);
            }
            return;

        } else {
            setExpense({ ...expense, [e.target.name]: sValue });
            setError(null);
        }

    };


    /**
     * Sets the selected date.
     * 
     * @param {date} createdAt - Selected date by user.
     * @prviate
     */
    const onDateChange = (createdAt) => {

        if (createdAt) {
            setExpense({ ...expense, createdAt: createdAt });
        }

    };


    /**
     * Changes the focus of the Single date picker.
     * 
     * @param {object} focused - provides boolean value.
     * @private
     */
    const onFocusChange = ({ focused }) => {
        setfocused(focused);
    };


    /**
     * Form submission.
     * 
     * @param {event} e - Triggered by user submit press.
     * @private
     */
    const onSubmit = (e) => {

        e.preventDefault();

        if (!expense.description || !expense.amount) {

            // Set error states.
            setError('Please provide a description & amount');

        } else {

            // Clear error states.
            setError(null);
            console.log("form submitted");

            // Submit form data.
            onFormSubmit({
                description: expense.description,
                amount: parseFloat(expense.amount, 10) * 100,
                createdAt: expense.createdAt.valueOf(),
                note: expense.note
            });

        }

    };

    return (

        <div className="expenseForm">
            <form onSubmit={(e) => onSubmit(e)}>
                <input
                    type="text"
                    name="description"
                    placeholder="Description"
                    autoFocus
                    value={expense.description}
                    onChange={(e) => onValueChange(e)}
                />
                <input
                    type="text"
                    name="amount"
                    placeholder="Amount"
                    value={expense.amount}
                    onChange={(e) => onValueChange(e)}
                />
                <SingleDatePicker
                    id="DatePicker"
                    date={expense.createdAt}
                    onDateChange={(e) => onDateChange(e)}
                    focused={focused}
                    onFocusChange={(e) => onFocusChange(e)}
                    numberOfMonths={1}
                    isOutsideRange={(day) => false}
                />
                <textarea 
                    name="note" 
                    onChange={(e) => onValueChange(e)} 
                    value={expense.note}
                    placeholder="Add a note for your expense (optional)"
                />
                <button className="btn">{ expenseId ? "Update expense" : "Add expense" }</button>
            </form>
            {
                error ? (
                    <div className="errorMessageBox">
                        <p>{error}</p>
                    </div>
                ) : null
            }
        </div>

    );

};

export default ExpenseForm;