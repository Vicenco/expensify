import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';

import { startLogout } from '../../actions/auth';
import { clearExpenses } from '../../actions/expenses';
import { clearIncome } from '../../actions/income';

import './header.styles.scss';

const Header = ({ startLogout, clearExpenses, clearIncome, userInfo }) => {

  const logout = () => {
    clearExpenses();
    clearIncome();
    startLogout();
  };

  return (
    <header className="noselect">
      <div className="container">
        <Link to="/">
          <h1>Expensify</h1>
        </Link>
        {
          userInfo && userInfo.user ? (
          <div className="userDetails">
            { userInfo.user.photoURL ? ( 
              <img src={userInfo.user.photoURL} alt={userInfo.user.displayName}/> ) : (
                <span>{userInfo.user.email}</span>
              )
            }            
            <button onClick={logout} className="logoutBtn">Log out <FontAwesomeIcon icon={faSignOutAlt} /></button>
          </div>
          ) : null
        }
      </div>
    </header>
  );

}

const mapDispatchToProps = (dispatch) => ({
  startLogout: () => dispatch(startLogout()),
  clearExpenses: () => dispatch(clearExpenses()),
  clearIncome: () => dispatch(clearIncome())
});

const mapStateToProps = (state) =>({
  userInfo: state.auth
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
