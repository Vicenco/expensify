import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import moment from 'moment';
import DraggableModal from '../DraggableModal';
import './monthPicker.styles.scss';

const MonthPicker = (props) => {

    const [currentYear, setYear] = useState(moment());
    const [data, setData] = useState([]);
    const [newSelection, setSelection] = useState(false);
    const allMonths = moment.monthsShort();


    useEffect(() => {

        if (newSelection) {

            setSelection(false);
            props.callBack(currentYear);

        }
        sortAvailableMonths();
    }, [currentYear]);


    /**
     * Filters out months that have data.
     * @private
     */
    const sortAvailableMonths = () => {

        let aMonths = []

        // Obtain all months and associated years.
        props.data.forEach(item => {
            aMonths.push({
                month: moment(item.createdAt).format('MMM'),
                year: moment(item.createdAt).format('YYYY')
            });
        });

        // Remove duplicate entries.
        aMonths = Array.from(new Set(aMonths.map(JSON.stringify))).map(JSON.parse);

        setData(aMonths);

    };


    /**
     * Increases the year by one.
     * @private
     */
    const _increaseYear = () => {
        setYear(prevYear => prevYear.clone().add(1, 'year'));
    };


    /**
     * Decreases the year by one.
     * @private
     */
    const _decreaseYear = () => {
        setYear(prevYear => prevYear.clone().subtract(1, 'year'));
    };


    /**
     * Sets the selected month on the currently specified year.
     * 
     * @returns {callback} callback function to parent component.
     * @private
     */
    const _setSelectedMonth = (e) => {

        if (e.currentTarget.className.includes("disabled")) {
            return;
        }

        const monthKey = parseInt(e._dispatchInstances.key);
        setYear(currentYear.clone().set('month', monthKey));
        setSelection(true);

    };


    /**
     * Sets the class names if a month element should be enabled/disabled.
     * 
     * @param {string} month - current month value.
     * @private
     */
    const _isEnabled = (month) => {

        const foundData = data.find(element => element.month === month && element.year === currentYear.format('YYYY'));

        if (foundData) {
            return "month";
        } else {
            return "month disabled";
        }

    };


    if (!props.show) {
        return null;
    }

    return ReactDOM.createPortal(
        <div className="dimmer">
            <DraggableModal width={600} height={1068}>
                <div className="monthPicker">
                    <div className="headerPicker">
                        <h2 className="title">
                            <span className="press" onClick={() => _decreaseYear()}>{'< '}</span>
                            <span className="yearLabel">{currentYear.format('YYYY')}</span>
                            <span className="press" onClick={() => _increaseYear()}>
                                {currentYear.clone().add(1, 'hour') > moment() ? '' : ' >'}
                            </span>
                        </h2>
                    </div>
                    <div className="content">
                        {
                            allMonths.map((month, idx) => (
                                <div className={_isEnabled(month)} key={idx} onClick={(e) => _setSelectedMonth(e)}>
                                    <span>{month}</span>
                                </div>
                            ))
                        }
                    </div>
                </div>
            </DraggableModal>
        </div>,
        document.querySelector('#monthPicker')
    );


};

export default MonthPicker;