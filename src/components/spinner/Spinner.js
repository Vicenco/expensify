import React from 'react';
import './loader.styles.scss';

const Spinner = () => (
    <div className="loader">
        <img className="loader__image" src="/assets/loader.gif" alt="loader" />
    </div>
);

export default Spinner;