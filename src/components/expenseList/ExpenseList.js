import React from 'react';
import { connect } from 'react-redux';
import ExpenseListItem from '../expenseListItem/ExpenseListItem';
import selectExpenses from '../../selectors/expenses';

import './expenseList.styles.scss';

const ExpenseList = ({ expenses, toast }) => (

  <div className="container noselect">
    <div className="expenseList">
      <div className="expenseListHeader">
        <span>Expense</span>
        <span>Amount</span>
      </div>
      <div className="listData">
        {expenses.length > 0 ? expenses.map((expense) => <ExpenseListItem key={expense.id} {...expense} showToast={toast}/>) 
        : (<span className="showNoData">You have no expenses</span>)}
      </div>
    </div>
  </div>

);

const mapStateToProps = (state) => ({
  expenses: selectExpenses(state.expenses, state.filters)
});

export default connect(mapStateToProps)(ExpenseList);
